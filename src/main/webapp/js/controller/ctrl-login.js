angular.module("loginApp", ["ngRoute"])
    .controller("LoginController", function($scope, $window, $http, $routeParams){

        $scope.accessToken = {
            code : "",
            grant_type : "authorization_code"
        }

        $scope.accessTokenUrl = "http://api.fazio.sensedia.com/oauth/access-token";

        $scope.clientId = "";
        $scope.clientSecret = "";

        $scope.loadClientId = function(){
             $scope.errors.clientId = false;
        }

        $scope.loadClientSecret = function(){
             $scope.errors.clientSecret = false;
        }

        $scope.errors = {
            clientId : false,
            clientSecret: false
        }

        $scope.initCallback = function(){
            var params = $window.location.search.split("=");

            var callbackCode = params[1].replace("&clientId", "");
            var clientId =  params[2].replace("&clientSecret", "");
            var clientSecret = params[3];

            var successCallback = function(result){
                $scope.token = result.data.access_token;
            }

            var errorCallback = function(result){

            }

            var encodedKeys = btoa(clientId + ":" + clientSecret);

            var config = {
                headers : {
                    "Content-Type"  : "application/json",
                    "Authorization" :  "Basic " + encodedKeys
                }
            }

            if(!angular.isUndefined(callbackCode) && callbackCode != ""){

                $scope.accessToken.code = callbackCode;

                $http.post($scope.accessTokenUrl, $scope.accessToken, config)
                     .then(successCallback, errorCallback);
            } else {
                return;
            }
        }

        $scope.loadApi = function(){

            var successCallback = function(result){
                $scope.api = result.data;
            }

            var errorCallback = function(reuslt){

            }

            var apiId = 1273;
            var apiManagerUrl = "http://forrester.sensedia.com/api-manager/api/v2/apis/" + apiId;

            var config = {
                headers: {
                    "Sensedia-Auth" : "cm9vdDptYW5hZ2Vy"
                }
            }

            $http.get(apiManagerUrl, config).then(successCallback, errorCallback);

        }

        $scope.callFazioLogin = function(){

            if($scope.clientId == null || $scope.clientId == ""){
                $scope.errors.clientId = true;
                return;
            }

            $window.open("http://" + $window.location.hostname + ":7070/fazio/login.html?clientId=" + $scope.clientId + "&clientSecret=" + $scope.clientSecret, "Fazio Login", "width=600, height=600");
        }

        $scope.callOperation = function(api, resource, operation){
            var success = function(result){
                $scope.resultCall = angular.toJson(result.data, true);
                 $("#modal-call-result").openModal();
            }

            var error = function(result){
                $scope.resultCall = result.data;
                $("#modal-call-result").openModal();
            }

            var config = {
                headers : {
                    "access-token" : $scope.token,
                    "client-id" : $scope.clientId
                }
            }

            if(operation.method == "GET"){
                //$http.get("http://localhost:8080/forrester/v1/getopenorders?customerId=999").then(success, error);

                $http.get("http://api.fazio.sensedia.com" + api.basePath + operation.path).then(success, error);
            }
        }

        $scope.closeModalResult = function(){
            $("#modal-call-result").closeModal();
        }

    });